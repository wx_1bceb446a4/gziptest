package com.jinchange.gzip.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jinchange.gzip.entity.Advertising;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @ClassName: ProjectController
 * @Author zhangjin
 * @Date 2022/3/24 20:41
 * @Description:
 */
@Slf4j
@RestController
public class AdvertisingController {

    @PostMapping("/save")
    public Advertising saveProject(@RequestBody Advertising advertising) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(advertising);
        log.info("获取请求内容为: " + json);
        return advertising;
    }
}
