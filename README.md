# 项目说明
* SpringBoot接口适配Gzip压缩数据请求的Demo

# 案例测试
## Postman 测试
### 请求路径和数据
![img.png](src/main/resources/img.png)
### 无需返回压缩内容
![img.png](src/main/resources/imgx.png)
### 需要返回压缩内容
![img_1.png](src/main/resources/img_1.png)

## HttpClient 测试
在测试用例 testGzipRequestOnHttpClient
